//
//  ViewController.swift
//  GifMaker
//
//  Created by Stanislav Volskyi on 8/16/17.
//  Copyright © 2017 Stanislav Volskyi. All rights reserved.
//

import Cocoa
import AVFoundation

class ViewController: NSViewController {
  @IBOutlet weak var tableView: NSTableView!
  
  @IBOutlet weak var dragAndDropLabel: NSTextField!
  @IBOutlet weak var dragAndDropView: DragNDropView!
  
  @IBOutlet weak var startTimeTextField: NSTextField!
  @IBOutlet weak var endTimeTextField: NSTextField!
  @IBOutlet weak var fpsTextField: NSTextField!
  @IBOutlet weak var loopCountTextField: NSTextField!
  
  @IBOutlet weak var createButton: NSButton!
  @IBOutlet weak var progressIndicator: NSProgressIndicator!
  
  var videoUrl: URL?
  var gifUrl: URL?
  var videoDuration = 0.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupFields()
    disableControls(true)
    dragAndDropView.delegate = self
    tableView.selectionHighlightStyle = .none
  }
  
  // MARK: Actions
  @IBAction func createButtonAction(_ sender: Any) {
    if (Double(endTimeTextField.stringValue) ?? 0) - (Double(startTimeTextField.stringValue) ?? 0) <= 0 {
      return
    }
    progressIndicator.isHidden = false
    progressIndicator.startAnimation(nil)
    let duration = Float(endTimeTextField.stringValue)! - Float(startTimeTextField.stringValue)!
    disableControls(true)
    
    Regift.createGIFFromSource(videoUrl!, startTime: Float(startTimeTextField.stringValue)!, duration: duration, frameRate: Int(fpsTextField.stringValue)!) { (url) in
      progressIndicator.isHidden = true
      gifUrl = url
      let panel = NSSavePanel()
      panel.delegate = self
      panel.runModal()
    }
  }
  
  @IBAction func textFieldValueChanged(_ sender: NSTextField) {
    if (Double(endTimeTextField.stringValue) ?? 0) - (Double(startTimeTextField.stringValue) ?? 0) > 0 {
      createButton.isEnabled = true
    } else {
      createButton.isEnabled = false
    }
  }
  
  // MARK: Private
  fileprivate func disableControls(_ disable: Bool) {
    startTimeTextField.isEnabled = !disable
    endTimeTextField.isEnabled = !disable
    fpsTextField.isEnabled = !disable
    loopCountTextField.isEnabled = !disable
    createButton.isEnabled = !disable
  }
  
  private func setupFields() {
    let formatter = NumberFormatter()
    startTimeTextField.formatter = formatter
    endTimeTextField.formatter = formatter
    
    let fpsFormatter = NumberFormatter()
    fpsFormatter.allowsFloats = false
    fpsFormatter.minimum = 5
    fpsFormatter.maximum = 15
    fpsTextField.formatter = fpsFormatter
    
    let loopsFormatter = NumberFormatter()
    loopsFormatter.allowsFloats = false
    loopsFormatter.minimum = 0
    loopsFormatter.maximum = 50
    loopCountTextField.formatter = loopsFormatter
  }
}

extension ViewController: DragNDropViewDelegate {
  func dragNDropView(_ view: DragNDropView, receivedVideo url: URL) {
    videoUrl = url
    disableControls(false)
    let asset = AVAsset(url: videoUrl!)
    videoDuration = CMTimeGetSeconds(asset.duration)
    tableView.reloadData()
    endTimeTextField.stringValue = "\(videoDuration)"
  }
}

extension ViewController: NSTableViewDataSource {
  func numberOfRows(in tableView: NSTableView) -> Int {
    return videoUrl == nil ? 0 : 1
  }
}

extension ViewController: NSTableViewDelegate {
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
    let cell = tableView.make(withIdentifier: "VideoCell", owner: nil) as! VideoCellView
    cell.nameLabel.stringValue = videoUrl!.lastPathComponent
    let asset = AVAsset(url: videoUrl!)
    let formatter = DateFormatter()
    formatter.dateFormat = "mm:ss"
    let date = Date(timeIntervalSince1970: videoDuration - Double(TimeZone.current.secondsFromGMT()))
    cell.durationLabel.stringValue = formatter.string(from: date)
    let time = CMTime(seconds: 3, preferredTimescale: asset.duration.timescale)
    var actualTime = kCMTimeZero
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
    imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
    guard let cgImage = try? imageGenerator.copyCGImage(at: time, actualTime: &actualTime) else {
      return cell
    }
    let image = NSImage(cgImage: cgImage, size: asset.tracks(withMediaType: AVMediaTypeVideo).first?.naturalSize ?? .zero)
    cell.thumbnailImageView.image = image
    return cell
  }
  
}

extension ViewController: NSOpenSavePanelDelegate {

  func panel(_ sender: Any, userEnteredFilename filename: String, confirmed okFlag: Bool) -> String? {
    if okFlag {
      guard let directory = (sender as! NSSavePanel).url else {
        return nil
      }
      var fullUrl = directory
      fullUrl.appendPathExtension("gif")
      try? FileManager.default.moveItem(at: gifUrl!, to: fullUrl)
      disableControls(false)

      return filename
    }
    return nil
  }
}

