//
//  VideoCellView.swift
//  GifMaker
//
//  Created by Stanislav Volskyi on 8/16/17.
//  Copyright © 2017 Stanislav Volskyi. All rights reserved.
//

import Cocoa

class VideoCellView: NSTableCellView {
  @IBOutlet weak var thumbnailImageView: NSImageView!
  @IBOutlet weak var nameLabel: NSTextField!
  @IBOutlet weak var durationLabel: NSTextField!

}
