//
//  DragNDropView.swift
//  GifMaker
//
//  Created by Stanislav Volskyi on 8/16/17.
//  Copyright © 2017 Stanislav Volskyi. All rights reserved.
//

import Cocoa

protocol DragNDropViewDelegate {
  func dragNDropView(_ view: DragNDropView, receivedVideo url: URL)
}

class DragNDropView: NSView {
  private var acceptableTypes: Set<String> { return [NSURLPboardType] }
  fileprivate let filteringOptions = [NSPasteboardURLReadingContentsConformToTypesKey: ["public.movie"]]
  var delegate: DragNDropViewDelegate?
  
  override init(frame frameRect: NSRect) {
    super.init(frame: frameRect)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: Setup
  private func setup() {
    register(forDraggedTypes: Array(acceptableTypes))
  }
  
  func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
    var canAccept = false
    let pasteBoard = draggingInfo.draggingPasteboard()
    if pasteBoard.canReadObject(forClasses: [NSURL.self], options: filteringOptions) {
      canAccept = true
    }
    return canAccept
  }
}

// MARK: NSDraggingDestination
extension DragNDropView {
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    if shouldAllowDrag(sender) {
      return .link
    }
    return NSDragOperation()
  }
  
  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let pasteBoard = sender.draggingPasteboard()
    if let urls = pasteBoard.readObjects(forClasses: [NSURL.self], options:filteringOptions) as? [URL], urls.count == 1 {
      delegate?.dragNDropView(self, receivedVideo: urls[0])
      return true
    }
    return false
  }
}
